import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner arrayScanner = new Scanner(System.in);

        //Fruit Array
        String[] newArray = {"apple","avocado","banana","kiwi","orange"};

        System.out.println(Arrays.toString(newArray));

        System.out.println("Which fruit would like to get the index of?");

        String fruit=arrayScanner.nextLine();
        int index = Arrays.binarySearch(newArray,fruit);

        System.out.println("The index of "+fruit+" is: "+index);

        //Friend ArrayList
        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John","Mark","Kyle","Janella"));

        System.out.println("My friends are: "+ friends);

        //HashMap inventory

        HashMap<String,Integer> inventory=new HashMap<>();
        inventory.put("toothpaste",15);
        inventory.put("toothbrush",25);
        inventory.put("soap",10);

        System.out.println("Our current inventory consists of:");
        System.out.println(inventory);
    }
}